Rancher-cli
------------------

Rancher client dockerized.

Usage:

```
docker run -v `pwd`:/rancher -e "RANCHER_URL=" -e "RANCHER_ACCESS_KEY=" -e "RANCHER_SECRET_KEY=" registry.gitlab.com/recalbox/ops/rancher-cli:master up
```
